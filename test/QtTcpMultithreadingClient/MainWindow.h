﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

namespace ZTcp {
class TcpClient_T;
}

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

 private slots:
  void on_btnTest0_clicked();

  void on_btnTest1_clicked();

  void on_btnTest2_clicked();

  void on_btnTest3_clicked();

 private:
  Ui::MainWindow *ui;
  ZTcp::TcpClient_T *client_;
};
#endif// MAINWINDOW_H
