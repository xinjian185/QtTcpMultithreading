﻿#include "MainWindow.h"

#include <QDebug>

#include "./ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow) {
  ui->setupUi(this);
}

MainWindow::~MainWindow() {
  delete ui;
}

std::function<void(QString)> f;
auto myLambda=[](QString msg){
  qDebug()<<"msg:"<<msg;
};
void MainWindow::on_btnTest0_clicked() {
  if (f) {
    f("123");
  } else {
    qDebug() << "no!!!";
    f=myLambda;
    f("456");
  }

}

void MainWindow::on_btnTest1_clicked() {
}

void MainWindow::on_btnTest2_clicked() {
}

void MainWindow::on_btnTest3_clicked() {
}
