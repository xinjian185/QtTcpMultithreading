# QtTcpMultithreading

#### 介绍
基于Qt开发的多线程tcp，可以在多线程安全使用

#### 软件架构
软件架构说明


#### 安装教程

1. 需要已经安装Qt，测试版本为Qt5.15.2；

2. 在环境变量中加入Qt的bin路径（qmake.exe所在路径）；

3. OPTION CompileTest 默认为ON，如果不需要编译，需要设置为OFF，在CPM中使用如下：

   ```cmake
   # 先加入组件
   include(CPM)
   
   # 拉去并使用库
   CPMAddPackage(
           # 库命名
           NAME QtTcpMultithreading
           # 库的地址
           GIT_REPOSITORY ssh://git@192.168.101.129:3022/libs/cpplibs/QtTcpMultithreading.git
           # 库的tag标签或者分支名字
           GIT_TAG v0.1
           # 设置options
           OPTIONS "CompileTest OFF"
   )
   
   ```

   

#### 使用说明

##### tcp server使用：

```c++
#include "QtTcpMultithreading.h"

// 用于记录新连接的socket
ZTcp::TcpClient_T *client_{nullptr};

// 用于新链接的socket收到信息时，进行处理的逻辑函数，替换为你的代码
void MainWindow::dealRead(const QByteArray &arg) {
  qDebug() << "receive:" << arg;
  static int num{0};
  QtConcurrent::run([=] {
    for (int i{0}; i != 10; ++i) {
      QByteArray msg{QString{"%1:%2"}.arg(num++).arg("world").toUtf8()};
      // 多线程中调用socket,可以在任意地方调用
      client_->write("world");
    }
  });
}

void MainWindow::on_btnTest0_clicked() {
  // 初始化 server
  ZTcp::TcpServer_T *server{new ZTcp::TcpServer_T{this}};
  // 监听地址、端口
  server->listen(QHostAddress{"127.0.0.1"}, 9966);
  // 当有新的请求连接时,触发sigNewClient信号
  connect(server, &ZTcp::TcpServer_T::sigNewClient, [=](ZTcp::TcpClient_T *client) {
    // 可以记录下来，在其他多线程中调用
    client_ = client;
    // 使用read注册回调函数dealRead
    client_->read([this](auto &&arg) { dealRead(std::forward<decltype(arg)>(arg)); });
  });
}

```

##### tcp client使用：

```c++
#include "QtTcpMultithreading.h"

// 用于记录连接的socket
ZTcp::TcpClient_T *client_{nullptr};

void MainWindow::on_btnTest0_clicked() {
  // 准备回复的消息
  QString msg{"myTest"};
  // 实例化client
  client_ = new ZTcp::TcpClient_T{this};
    
  // 绑定client自身的ip和端口
  // client_->bind(QHostAddress{"127.0.0.1"},18899);
    
  // 设置读取到消息时的处理函数
  client_->read([](QByteArray msg) {
    qDebug() << "client receive:" << msg;
  });

  // 连接到server
  client_->connectToHost(QHostAddress{"127.0.0.1"}, 9966);
  // 启动
  client_->start();
}

void MainWindow::on_btnTest1_clicked() {
  // 使用记录的client在多线程发送消息
  QtConcurrent::run([=]{
    client_->write("client hello");
  });
}
```

