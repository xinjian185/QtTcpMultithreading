﻿/**
 * @brief 用于多线程的tcp server
 * @author Zr
 * @date 20230221
 */
#pragma once

#include <QTcpServer>

#include "QtTcpMultithreading_global.h"

namespace ZTcp {
class TcpClient_T;
/**
* @brief 用于多线程的tcp server
*/
class QTTCPMULTITHREADING_EXPORT TcpServer_T : public QTcpServer {
  Q_OBJECT
 public:
  explicit TcpServer_T(QObject *parent = nullptr);

 protected:
  /**
   * @brief 重写新链接时处理函数
   * @param handle socket底层handle
   */
  void incomingConnection(qintptr handle) override;

 private:
  // 自带的函数,使得无法调用,强制性调用自定义的信号槽
  bool hasPendingConnections() const override;
  QTcpSocket *nextPendingConnection() override;
  // 强行覆盖掉以前的信号,使用sigNewClient替换
  void newConnection();

 signals:
  void sigNewClient(TcpClient_T *client);
};
}// namespace ZTcp
