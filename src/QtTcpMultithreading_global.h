﻿#pragma once

#include <QtCore/qglobal.h>

#if defined(QTTCPMULTITHREADING_LIBRARY)
#  define QTTCPMULTITHREADING_EXPORT Q_DECL_EXPORT
#else
#  define QTTCPMULTITHREADING_EXPORT Q_DECL_IMPORT
#endif

