﻿#include "TcpServer_T.h"

#include <QEventLoop>

#include "TcpClient_T.h"

using namespace ZTcp;

TcpServer_T::TcpServer_T(QObject *parent) : QTcpServer(parent) {
}

void TcpServer_T::incomingConnection(qintptr handle) {
  // 初始化client
  auto client{new TcpClient_T{this, false, handle}};
  // 启动
  client->start();

  // 等待初始化完成
  QEventLoop loop;
  connect(client, &TcpClient_T::sigStartDone, &loop, &QEventLoop::quit);
  loop.exec();

  // 发送
  emit sigNewClient(client);
}

bool TcpServer_T::hasPendingConnections() const {
  return QTcpServer::hasPendingConnections();
}

QTcpSocket *TcpServer_T::nextPendingConnection() {
  return QTcpServer::nextPendingConnection();
}

void TcpServer_T::newConnection() {
  QTcpServer::newConnection();
}
